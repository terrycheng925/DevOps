from flask import Flask, render_template, request, redirect, url_for
import requests 



app = Flask(__name__)

@app.route('/')
def index():
        
    response = requests.get("https://api.nasa.gov/planetary/apod?api_key=5WPcKKgM7lqZnWZhDq2eVQAwgc71N7eQKHTSKKJy")
    #return render_template('index.html', landing_image='https://via.placeholder.com/500')
    print(response.json())
    return render_template('index.html', landing_image=response.json["url"])

@app.route('/mars')
def mars():
    return render_template('mars.html')